pallas = {}
pallas.initialized = false

function pallas:Initialize()
    if pallas.initialized then return end

    -- initialize engine
    pallas.engine:Initialize()

    pallas.initialized = true
end

local frame = CreateFrame("FRAME")
frame:RegisterEvent("ADDON_LOADED")
frame:RegisterUnitEvent("PLAYER_ENTERING_WORLD")

function frame:OnEvent(event, ...)
    if event == "ADDON_LOADED" then
        addon_name = ...

        if addon_name ~= "Pallas" then return end
    end

    if event == "PLAYER_ENTERING_WORLD" then
        print('PLAYER_ENTERING_WORLD')
        if not pallas.initialized then
            pallas:Initialize()
        end
    end
end
frame:SetScript("OnEvent", frame.OnEvent)
