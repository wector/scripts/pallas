# Pallas (ALPHA)

Pallas is currently in ALPHA-phase which mean this README is useless and you are too.

## Getting Started

Follow these instructions to install and use Pallas.

### Prerequisites

The only prerequisite is having a working copy of Wector and credentials to use it. If you don't have this, kindly leave this forsaken place now.

### Installing

TODO: Add more descriptive installation process options like cloning vs. ZIP download. Think ELI5

Download this repository using the method of your liking then place the downloaded script into the scripts directory at the root of Wector. Optionally rename the Pallas folder from `Pallas-master` to `Pallas` if you downloaded it as a ZIP.

After installation the path to Pallas will be `<Wector root>/scripts/Pallas`

## Contribute

Pallas is a community driven script for Wector, therefore it rests on contributions from users to keep alive and well. If you wish to contribute to this project there are some things you need to consider.

Please read [CONTRIBUTING.md](CONTRIBUTE.md) for details on our code of conduct, the process for submitting pull requests to us, and recommended coding styles.
